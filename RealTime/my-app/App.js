import React from 'react'
import BottomTab from './src/routes/BottomTab';
import { LogBox } from 'react-native';

LogBox.ignoreLogs(['Setting a timer']);

export default function App() {
    return (
     <BottomTab />
    )
}