import { firebase } from '@firebase/app'

const CONFIG = {
  apiKey: "AIzaSyAuO12M0b5ouCp4jsbPuivGBq717wiIJVU",
  authDomain: "my-app-ec8c0.firebaseapp.com",
  databaseURL: "https://my-app-ec8c0-default-rtdb.firebaseio.com",
  projectId: "my-app-ec8c0",
  storageBucket: "my-app-ec8c0.appspot.com",
  messagingSenderId: "191386557411",
  appId: "1:191386557411:web:aec3a49c691f66ba285ed6"
};
firebase.initializeApp(CONFIG)

export default firebase;
