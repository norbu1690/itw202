import React from "react";
import { View, Text } from "react-native";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'lightblue'
    },
    welcome: {
        frontSize: 20,
        textAlign: 'center',
        margin: 10
    }
});
export default styles;