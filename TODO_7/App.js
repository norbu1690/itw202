import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';

export default function App() {
  return (
    <View style={styles.container}>
      <View style={styles.square1} />
      <View style={styles.square2} />
    </View>
  );}
const styles = StyleSheet.create({
  container: {
    backgroundColor: "white",
    flex: 1,
    alignItems: "center",
    justifyContent: "flex-end",
    flexDirection: "column",
    marginBottom: 50
  },
  square1: {
    backgroundColor: "red",
    width: 100,
    height: 100,
  },
  square2: {
    backgroundColor: "purple",
    width: 100,
    height: 100,
  },
});
