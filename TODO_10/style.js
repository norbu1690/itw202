import React from "react";
import { Text, View, StyleSheet } from 'react-native';

const style = StyleSheet.create({
    txtInput: {
        borderColor: 'gray', 
        borderWidth: 1
    }
});
export default style;