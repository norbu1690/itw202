import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import React from 'react';
// import React, { useState, useContext } from "react"; 

export default class App extends React.Component {
  render = () => (
    <View style={styles.container}>
      <Text>Practical 2</Text>
      <Text>Stateless and staefull Components</Text>
      <Text style={styles.text}>
        You are ready to start the journey.
      </Text>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    padding: '10%',
    justifyContent: 'center',
  },
  text: {
    marginTop: '5%',
  }
});
