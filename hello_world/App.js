import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import { add, multiply } from './components/nameExport';
import division from './components/defaultExport';
import Courses from './components/funcComponent';

export default function App() {
  return (
    <View style={styles.container}>
      <Text>Hello world</Text>
      <Text>Result of Addition: {add(5, 6)}</Text>
      <Text>Result of Multiplication: {multiply(5, 8)}</Text>
      <Text>Result of Division: {division(10,2)}</Text>
      <StatusBar style="auto" />
      <Courses></Courses>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'lightblue',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
