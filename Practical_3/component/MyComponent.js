import React from "react";
import { View, StyleSheet, Text } from "react-native";


const MyComponent = () => {
    const greeting = 'Sonam Wangmo';
    const greeting1 = <Text>Jigme Wangmo</Text>
    return (
        <View>
            <Text style = {style.textStyle}> This is a demo of JSK </Text>
            <Text>Hi There!!! {greeting}</Text>
            {greeting1}
        </View>
    )
};

const style = StyleSheet.create({
    textStyle: {
        fontSize: 24
    }
});
export default MyComponent;