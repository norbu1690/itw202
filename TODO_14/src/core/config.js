

export const firebaseConfig = {
  apiKey: "AIzaSyBHYbS4E6k7EdvVcqcpbchAtAP6St20gro",
  authDomain: "practical-32375.firebaseapp.com",
  databaseURL: "https://practical-32375-default-rtdb.firebaseio.com",
  projectId: "practical-32375",
  storageBucket: "practical-32375.appspot.com",
  messagingSenderId: "862790058652",
  appId: "1:862790058652:web:2baa9d2f1c9f4dccdde359",
  measurementId: "G-9K3HMWRWQ6"
};

passwordReset: email => {
  return firebase.auth().sendPasswordResetEmail(email)
}
