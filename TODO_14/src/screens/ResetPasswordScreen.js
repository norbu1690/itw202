import React, { useState } from "react";
import { View, StyleSheet, Text, TouchableOpacity } from "react-native";
import Button from "../components/Button";
import Header from "../components/Header";
import Background from "../components/Background";
import TextInput from "../components/TextInput";
import { emailValidator } from '../core/helpers/emailValidator';
import BackButton from "../components/BackButton";
import { resetPassword } from "../api/auth-api"

export default function ResetPasswordScreen({navigation}) {
    const [email, setEmail] = useState({ value: "", error: ""})
    const [loading, setLoading] = useState();

    const onSubmitPressed = async() => {
        const emailError = emailValidator(email.value);
        if (emailError) {
            setEmail({ ...email, error: emailError });
        }
        setLoading(true)

        const response = await resetPassword(email.value);

        if (response.error) {
            alert(response.error);
        }
        else {
            alert('Check your email for a reset link')
            navigation.navigate('LoginScreen') 
        }
        setLoading(false)
    }

    const onCancelPressed = () => {
        navigation.navigate('LoginScreen')
    }
    return (
        <Background>
            <BackButton goBack={navigation.goBack} />
            <Header>Restore Password</Header>
            <TextInput 
                label="Email" 
                value={email.value}
                placeholder='Enter email'
                autoCapitalize='none'
                error={email.error}
                errorText={email.error}
                onChangeText={(text) => setEmail({ value: text, error: "" })}
                description="Password reset email sent successfully"
            />
            <Button loading = {loading} mode="contained" onPress = {onSubmitPressed}>Submit</Button> 
            <Button mode="contained" onPress = {onCancelPressed}>Cancel</Button>
        </Background>
    )
}
