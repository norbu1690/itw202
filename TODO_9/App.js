import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Image } from 'react-native';

export default function App() {
  return (
    <View>
        <Image
          style={{width: 50, height:100, margin: '15%'}}
          source={{uri:'https://picsum.photos/100/100'}}
        />
        <Image
        style={{width: 50, height: 100, margin: '15%'}}
        source={require('../TODO_9/assets/logo.png')}
        />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'red',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    marginTop: 50
  }
});