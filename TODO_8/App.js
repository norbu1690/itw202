import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import My_Component from './components/My_Component';

export default function App() {
  return (
    <View style={styles.container}>
      <My_Component />
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
});
