import React from "react";
import { Text, View, StyleSheet } from 'react-native';

const MyComponent = () => {
    // var, let can be used inplace of const
    const greet = 'Getting started\nwith react\nnative!';
    const name = 'My name is Norbu\nDorji';
    return (
        <View>
            <Text style = {style.textStyle}>{greet}</Text>
            <Text style = {{ fontSize: 20, textAlign: 'center', paddingRight: 80, fontWeight: 'bold'}}>{name}</Text>
        </View>
    )
};

const style = StyleSheet.create({
    textStyle: {
        fontSize: 45,
        textAlign: 'center',
        paddingRight: 60
    }
});
export default MyComponent;