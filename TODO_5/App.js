import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import MyComponent from './component/MyComponent';

export default function App() {
  return (
    <View style={styles.container}>
      <MyComponent></MyComponent>
    </View>
    
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'white',
    alignItems: 'center',
    // cross axis
    justifyContent: 'center',
    // main axis
  },
});