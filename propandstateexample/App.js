import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import Courses from './component/courses';
import CountClass from './component/CountClass';
export default function App() {
  return (
    <View style={styles.container}>
      {/*<Courses></Courses>*/}
      <CountClass />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
