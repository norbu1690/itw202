import { StyleSheet, Text, View, ImageBackground, SafeAreaView } from 'react-native';
import StartGameScreen from './Screens/StartGameScreen';
import { LinearGradient } from 'expo-linear-gradient';
import { useState } from 'react';
import GameScreen from './Screens/GameScreen';
import GameOverScreen from './Screens/GameOverScreen';
import { useFonts } from 'expo-font';
import AppLoading from 'expo-app-loading';

export default function App() {
  const [userNumber, setUseNumber] = useState();
  const [gameIsOver, setGameIsOver] = useState(true)
  const [guessRounds, setGuessRounds] = useState(0)

  const [fontsLoaded] = useFonts({
    'open-sans': require('./assets/fonts/OpenSans-Regular.ttf'),
    'open-sans': require('./assets/fonts/OpenSans-Bold.ttf'),
  })

  if (!fontsLoaded) {
    return <AppLoading />
  }

  function pickedNumberHandler(pickedNumber) {
    setUseNumber(pickedNumber);
    setGameIsOver(false);
  }

  function gameOverHandler(numberOfRounds) {
    setGameIsOver(true);
    setGuessRounds(numberOfRounds)
  }

  function startNewGameHandler() {
    setUseNumber(null);
    setGuessRounds(0);
  }

  let screen = <StartGameScreen onPickNumber = {pickedNumberHandler} />
   if (userNumber) {
     screen =<GameScreen userNumber={userNumber} onGameOver = {gameOverHandler}/>
   }
   if (gameIsOver && userNumber) {
     screen = <GameOverScreen 
                userNumber={userNumber}
                roundsNumber={guessRounds}
                onStartNewGame={startNewGameHandler}
              />
   }
  return (
    <LinearGradient colors={['#4e0329','#ddb52fr']} 
    style={styles.container}>
      <ImageBackground
        source={require('./assets/Image/background.png')}
        resizeMode= 'cover'
        style={styles.backImage}
        imageStyle={styles.backgroundImage}>

      </ImageBackground>
      <SafeAreaView style={styles.container}>
        {screen}
      </SafeAreaView>
    </LinearGradient>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'lightblue',
    // alignItems: 'center',
    // justifyContent: 'center',
  },
  backImage: {
    // width: 500,
    // height: 1000,
    // position: 'absolute',
    opacity: 0.55,
  },
  // backgroundImage: {
  //   opacity: 0.15,
  // }
});
