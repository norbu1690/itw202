import { StyleSheet, Text, View , Alert, FlatList, useWindowDimensions} from 'react-native'
import React, { useEffect, useState } from 'react'
import Title from '../Component/ui/Title'
import NumberContainer from '../Component/game/NumberContainer';
import PrimaryButton from '../Component/ui/PrimaryButton';
import Card from '../Component/ui/Card';
import InstructionText from '../Component/ui/InstructionText';
import {Ionicons} from '@expo/vector-icons'
import GuessLogItem from '../Component/game/GuessLogItem';

function generateRandomBetween(min, max, exclude) {
    const rndNum = Math.floor(Math.random() * (max - min)) + min;

    if (rndNum == exclude) {
        return generateRandomBetween(min, max, exclude);
    }
    else{
        return rndNum;
    }
}

let minBoundary = 1;
let maxBoundary = 100;

function GameScreen ({userNumber, onGameOver}) {
    const initialGuess = generateRandomBetween(1, 100, userNumber )
    const [currentGuess, setCurrentGuess] = useState(initialGuess)
    const [guessRounds, setGuessRounds] = useState([initialGuess]);
    const {width, height} = useWindowDimensions();
    
useEffect(() => {
  if (currentGuess === userNumber) {
    onGameOver(guessRounds.length);
  }
},[currentGuess, userNumber, onGameOver])

useEffect(() => {
  minBoundary = 1;
  maxBoundary = 100;
}, [])

function nextGuessHandler(direction){
  if(
      (direction === 'lower' && currentGuess < userNumber) ||
      (direction === 'greater' && currentGuess > userNumber))
      {
        Alert.alert("Don't lie!", 'You know that this wrong...',[
          {text: 'Sorry', style: 'cancel'}
        ])
        return;
      }
      if (direction === 'lower'){
        maxBoundary = currentGuess;
      }
      else{
        minBoundary = currentGuess + 1 ;
      }
      console.log(minBoundary, maxBoundary)
      const newRndNumber = generateRandomBetween(minBoundary, maxBoundary, currentGuess)
      
      setCurrentGuess(newRndNumber);
      setGuessRounds((prevGuessRounds => [newRndNumber, ...prevGuessRounds]))
}
const guessRoundsListLength = guessRounds.length

  let content = 
  <>
    <NumberContainer>{currentGuess}</NumberContainer>
      <Card>
        <InstructionText style={styles.instructionText}>Higher or lower?</InstructionText>
        <View style={styles.buttonsContainer}>
        <View style={styles.buttonContainer}>
              <PrimaryButton onPress={nextGuessHandler.bind(this, 'greater')}>
                <Ionicons name='md-add' size={24} color='white' />
              </PrimaryButton>
            </View>
            <View style={styles.buttonContainer}>
              <PrimaryButton onPress={nextGuessHandler.bind(this, 'lower')}>
                <Ionicons name='md-remove' size={24} color='white' />  
              </PrimaryButton>
            </View>
        </View>
      </Card>
  </>

    if (width > 500) {
      content = (
        <>
          <View style={styles.buttonsContainerWide}>
            <View style={styles.buttonContainer}>
              <PrimaryButton onPress={nextGuessHandler.bind(this, 'greater')}>
                <Ionicons name='md-add' size={24} color='white' />
              </PrimaryButton>
            </View>
            <NumberContainer>{currentGuess}</NumberContainer>
            <View style={styles.buttonContainer}>
              <PrimaryButton onPress={nextGuessHandler.bind(this, 'lower')}>
                <Ionicons name='md-remove' size={24} color='white' />  
              </PrimaryButton>
            </View>
          </View>
        </>
      )
    }
  return (
    <View style ={styles.screen}>
        <Title>Opponent's screen</Title>
        <NumberContainer>{currentGuess}</NumberContainer>
      <Card>
          <InstructionText style={styles.instructionText}>Higher or Lower?</InstructionText>
          <View style={styles.buttonsContainer}>
            <View style={styles.buttonContainer}>
              <PrimaryButton onPress={nextGuessHandler.bind(this, 'greater')}>
                <Ionicons name='md-add' size={24} color='white' />
              </PrimaryButton>
            </View>
            <View style={styles.buttonContainer}>
              <PrimaryButton onPress={nextGuessHandler.bind(this, 'lower')}>
                <Ionicons name='md-remove' size={24} color='white' />  
              </PrimaryButton>
            </View>
          </View>
      </Card>
      <View style={styles.listContainer}>
        <FlatList
          data={guessRounds}
          renderItem={(itemData) => 
            <GuessLogItem 
              roundNumber={guessRoundsListLength - itemData.index}
              guess = {itemData.item}
            />  
          }  
          keyExtractor={(item) => item}
        
        //  <Text>{itemData.item}</Text>}
        //  keyExtractor={(item) => item}
        />
        {/* {guessRounds.map(guessRound => <Text key={guessRound}>{guessRound}</Text>)} */}
      </View>
    </View>
  )
}

export default GameScreen

const styles = StyleSheet.create({
  instructionText: {
    marginBottom: 12
  },  
  screen: {
      flex: 1,
      padding: 35,
      paddingTop: 45,
      alignItems: 'center'

  },
  buttonsContainer: {
    flexDirection: 'row'
  },
  buttonContainer: {
    flex:1
  },
  listContainer: {
    flex:1,
    padding: 12
  },
  buttonsContainerWide: {
    flexDirection: 'row',
    alignItems: 'center'
  }
})