import { Platform, StyleSheet, Text, View } from 'react-native'
import React from 'react'
import Colors from '../../constants/colors'

const Title = ({children}) => {
  return (
    <View>
      <Text style={styles.title}>{children}</Text>
    </View>
  )
}

export default Title

const styles = StyleSheet.create({
    title: {
      fontFamily: 'open-sans-bold',
      borderWidth: 2,
      borderColor: 'white',
      textAlign: 'center',
      borderWidth: Platform.OS === 'android' ? 2 : 0,
      color: 'white',
      fontSize: 24,
      // fontWeight:'bold',
      padding: 12,
      maxWidth: '80%',
      width: 300
    }
})