import { StyleSheet, Text, View, Pressable } from 'react-native'
import React from 'react'

function PrimaryButton ({ children, onPress }) {
  return (
    <View style={styles.buttonOuterContainer}>
        <Pressable android_ripple={{color: '#640233'}} 
            style={({pressed}) => pressed ? [styles.buttonInnerContainer, styles.pressed] : styles.buttonInnerContainer}
            onPress={onPress}
        >
            <Text styles={styles.buttonText}>{children}</Text> 
        </Pressable>
    </View>
  )
}

export default PrimaryButton

const styles = StyleSheet.create({
    buttonOuterContainer: {
        borderRadius: 28,
        margin: 4,
        overflow: 'hidden'
    },
    buttonInnerContainer: {
        backgroundColor: '#72063c',
        paddingVertical: 8,
        paddingHorizontal: 16,
        elevation: 2
    },
    buttonText: {
        color: 'red',
        textAlign: 'center'        
    },
    pressed: {
        opacity: 0.75
    }
})